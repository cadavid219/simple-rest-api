<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;

class StoreController extends Controller
{
    /**
     * Retrieves all the stores
     */
    public function index()
    {
        $stores = Store::all(['id', 'address', 'name']);
        $response_sucess_multiple = [
            "stores" => $stores,
            "success" => true,
            "total_elements" => count($stores)
        ];
        return response()->json($response_sucess_multiple, 200);
    }
}
