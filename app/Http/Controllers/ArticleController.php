<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Article;
use App\Store;

class ArticleController extends Controller
{
    /**
     * Retrieves all the articles
     */
    public function index()
    {
        $articles = Article::join('stores', 'articles.store_id', '=', 'stores.id')
            ->select(
                'articles.id',
                'articles.description',
                'articles.name',
                'articles.price',
                'articles.total_in_shelf',
                'articles.total_in_vault',
                'stores.name as store_name'
            )->orderBy('id', 'asc')
            ->get()
            ->toArray();

        $response_sucess_multiple = [
            "articles" => $articles,
            "success" => true,
            "total_elements" => count($articles)
        ];
        return response()->json($response_sucess_multiple, 200);
    }

    /**
     * Retrieves all the articles present in store with id $id
     *
     * @param string $id
     *    The id of the store to retrieve the articles for
     */
    public function articlesInStore(string $id)
    {
        if (!is_numeric($id)) {
            $response = [
                "success" => false,
                "error_code" => 400,
                "error_msg" => "Bad Request"
            ];
            return response()->json($response, 200);
        }

        $articles = Article::join('stores', 'articles.store_id', '=', 'stores.id')
            ->where('articles.store_id', $id)
            ->select(
                'articles.id',
                'articles.description',
                'articles.name',
                'articles.price',
                'articles.total_in_shelf',
                'articles.total_in_vault',
                'stores.name as store_name'
            )->orderBy('id', 'asc')
            ->get()
            ->toArray();
        if (count($articles) > 0 ){
            $response = [
                "articles" => $articles,
                "success" => true,
                "total_elements" => count($articles)
            ];
        } else {
            $response = [
                "success" => false,
                "error_code" => 404,
                "error_msg" => "Record not Found"
            ];
        }

        return response()->json($response, 200);
    }
}
