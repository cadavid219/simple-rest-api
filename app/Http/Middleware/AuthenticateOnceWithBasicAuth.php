<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateOnceWithBasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $auth = $request->headers->get('Authorization');

        if (is_null($auth) || !$this->isValidAuthHeader($auth)) {
            $response = [
                "success" => false,
                "error_code" => 401,
                "error_msg" => "Not authorized"
            ];
            return response()->json($response, 200);
        }
        return $next($request);
    }

    private function isValidAuthHeader(string $header) {
        $headerArray = explode(" ", $header);
        if (count($headerArray) < 2) {
            return false;
        }
        $decodedCredentials = base64_decode($headerArray[1]);
        return ($decodedCredentials && $decodedCredentials === 'my_user:my_password');
    }
}
