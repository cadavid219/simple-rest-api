<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'description',
        'name',
        'price',
        'total_in_shelf',
        'total_in_vault'
    ];
}
