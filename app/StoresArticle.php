<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoresArticle extends Model
{
    protected $fillable = [
        'store_id',
        'article_id'
    ];
}
