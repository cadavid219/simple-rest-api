# Usage

## Prerequisites
1. The app uses laravel 5.6 which requires PHP7.1+ and composer
2. A DB is required as well (only the DB). The configurations for the DB must be added to a file `.env`, which can be created and customized based on the existing `.env.example` (`cp .env.example .env`) 

## Installation
1. Clone or download the project
2. Run `composer install`
3. Run `php artisan migrate:refresh --seed` (This will create the tables and will fill them with dummy data)
4. run `php artisan serve`
 

 ## Notes
 The app uses a basic authentication method. To get data from the requests the header

 `Authorization: Basic <base64encoded__credentials>`

 must be present. Also remember that the value of credentials being validated is `my_user:my_password`.
An example of the header to add follows:

`Authorization: Basic bXlfdXNlcjpteV9wYXNzd29yZA==`