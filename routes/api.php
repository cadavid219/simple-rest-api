<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
 * Group all the routes used for the API
 */
Route::group(['middleware' => ['services']], function () {
    Route::get('stores', 'StoreController@index');
    Route::get('articles', 'ArticleController@index');
    Route::get('articles/stores/{id}', 'ArticleController@articlesInStore');
});
