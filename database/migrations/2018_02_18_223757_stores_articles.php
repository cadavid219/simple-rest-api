<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StoresArticles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores_articles', function (Blueprint $table) {
            $table->timestamps();
            $table->unsignedInteger('store_id');
            $table->unsignedInteger('article_id');
            $table->primary(['store_id', 'article_id']);
            $table->foreign('store_id')->references('id')->on('stores');
            $table->foreign('article_id')->references('id')->on('articles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores_articles');
    }
}
