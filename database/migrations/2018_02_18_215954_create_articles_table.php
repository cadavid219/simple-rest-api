<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->timestamps();
            $table->increments('id');
            $table->text('description');
            $table->string('name');
            $table->float('price');
            $table->integer('total_in_shelf');
            $table->integer('total_in_vault');
            // This is added based on the assumption that an article can only be realted to one store at the same time
            // If this is not the case that is why the stores_articles migration was created
            $table->unsignedInteger('store_id');
            $table->foreign('store_id')->references('id')->on('stores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
