<?php

use Illuminate\Database\Seeder;
use App\StoresArticle;

class StoresArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        $storeIds = DB::table('stores')->pluck('id')->toArray();
        $articleIds = DB::table('articles')->pluck('id')->toArray();
        for ($i = 0; $i < 100; $i++) {
            try {
                StoresArticle::create([
                    'store_id'       => $faker->randomElement($storeIds),
                    'article_id'     => $faker->randomElement($articleIds),
                ]);
            } catch (Exception $e) {}
        }
    }
}
