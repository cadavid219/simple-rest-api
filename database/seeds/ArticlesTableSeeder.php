<?php

use Illuminate\Database\Seeder;
use App\Article;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        $storeIds = DB::table('stores')->pluck('id')->toArray();
        for ($i = 0; $i < 50; $i++) {
            try {
                Article::create([
                    'description' => $faker->paragraph,
                    'name'  => $faker->name,
                    'price' => $faker->randomFloat(2, 100, 10000),
                    'total_in_shelf' => $faker->randomNumber(3),
                    'total_in_vault' => $faker->randomNumber(2),
                    // Only for when an article is related to one store at the same time
                    'store_id'       => $faker->randomElement($storeIds),
                ]);
            } catch (Exception $e) {}
        }
    }
}
